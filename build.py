import os

def build(gen, env):
    env = env.clone()

    env['CPPFLAGS'] += ['-DHAVE_CONFIG_H']
    env['CPPPATH'] += ['src/libs/libjpeg']

    # shut off warnings
    env['CFLAGS'] += [
        '-Wno-sign-conversion',
        '-Wno-unused-parameter',
        '-Wno-implicit-fallthrough',
    ]

    files = [
        'cjpeg.c', 'rdppm.c', 'rdgif.c', 'rdtarga.c', 'rdrle.c', 'rdbmp.c', 'rdswitch.c',
        'cdjpeg.c', 'jaricom.c', 'jcapimin.c', 'jcapistd.c', 'jcarith.c', 'jccoefct.c', 'jccolor.c',
        'jcdctmgr.c', 'jchuff.c', 'jcinit.c', 'jcmainct.c', 'jcmarker.c', 'jcmaster.c', 'jcomapi.c',
        'jcparam.c', 'jcprepct.c', 'jcsample.c', 'jctrans.c', 'jdapimin.c', 'jdapistd.c',
        'jdarith.c', 'jdatadst.c', 'jdatasrc.c', 'jdcoefct.c', 'jdcolor.c', 'jddctmgr.c',
        'jdhuff.c', 'jdinput.c', 'jdmainct.c', 'jdmarker.c', 'jdmaster.c', 'jdmerge.c',
        'jdpostct.c', 'jdsample.c', 'jdtrans.c', 'jerror.c', 'jfdctflt.c', 'jfdctfst.c',
        'jfdctint.c', 'jidctflt.c', 'jidctfst.c', 'jidctint.c', 'jquant1.c', 'jquant2.c',
        'jutils.c', 'jmemmgr.c', 'jmemnobs.c'
    ]

    lib = env.static_lib(gen, out = 'libjpeg', ins = files)
    env.install(gen, env['LIBDIR'], lib)
